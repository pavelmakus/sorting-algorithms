function sortNormal(array: number[]) {
  let gap = Math.floor(array.length / 2);

  while (gap >= 1) {
    for (let i = gap; i < array.length; i++) {
      const current = array[i];
      let j = i;

      while (j > 0 && current < array[j - gap]) {
        array[j] = array[j - gap];
        j -= gap;
      }

      array[j] = current;
    }


    gap = Math.floor(gap / 2);
  }

  return array;
}
