function sortNormal(array: number[]) {
 const factor = 1.247;
 let gapFactor = array.length / factor;

 while (gapFactor > 1) {
   const gap = Math.round(gapFactor);

   for (let i = 0, j = gap; j < array.length; i++, j++) {
     if (array[i] > array[j]) {
       [array[i], array[j]] = [array[j], array[i]]
     }
   }

   gapFactor = gapFactor / factor;
 }

 return array;
}
