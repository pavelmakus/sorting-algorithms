function sortNormal(array: number[]) {
  for (let i = 1; i < array.length; i++) {
    const current = array[i];
    let j = i;

    while (j > 0 && current < array[j - 1]) {
      array[j] = array[j - 1];
      j--;
    }

    array[j] = current;
  }

  return array;
}
