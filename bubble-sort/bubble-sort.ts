function sortNormal(array: number[]) {
  for (let i = 0, endI = array.length - 1; i < endI; i++) {
    for (let j = 0, endJ = endI - i; j < endJ; j++) {
      if (array[j] > array[j + 1]) {
        [array[j], array[j + 1]] = [array[j + 1], array[j]];
      }
    }
  }

  return array;
}

function sortOptimized(array: number[]) {
  let wasSwap = false;

  for (let i = 0, endI = array.length - 1; i < endI; i++) {
    for (let j = 0, endJ = endI - i; j < endJ; j++) {
      if (array[j] > array[j + 1]) {
        [array[j], array[j + 1]] = [array[j + 1], array[j]];
        wasSwap = true;
      }
    }

    if (!wasSwap) break;
  }

  return array;
}
