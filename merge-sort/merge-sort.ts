function merge(left: number[], right: number[]) {
  const array = [];
  let i = 0;
  let j = 0;

  while (i < left.length && j < right.length) {
    const item = left[i] < right[j] ? left[i++] : right[j++];
    array.push(item);
  }

  return [...array, ...left.slice(i), ...right.slice(j)];
}

function mergeSort(array: number[]) {
  if (array.length <= 1) {
    return array;
  }

  const middle = Math.floor(array.length / 2);
  const left = array.slice(0, middle);
  const right = array.slice(middle);

  return merge(mergeSort(left), mergeSort(right));
}
