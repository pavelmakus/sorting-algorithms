function sortNormal(array: number[]) {
  let left = 0;
  let right = array.length - 1;

  while (left < right) {
    for (let i = left; i < right; i++) {
      if (array[i] > array[i + 1]) {
        [array[i], array[i + 1]] = [array[i + 1], array[i]];
      }
    }

    right--;

    for (let i = right; i > left; i--) {
      if (array[i] < array[i - 1]) {
        [array[i], array[i - 1]] = [array[i - 1], array[i]];
      }
    }

    left++;
  }

  return array;
}

function sortOptimized(array: number[]) {
  let firstSwap = 0;
  let lastSwap = array.length - 1;

  let left = firstSwap;
  let right = lastSwap;

  while (left < right) {
    for (let i = left; i < right; i++) {
      if (array[i] > array[i + 1]) {
        [array[i], array[i + 1]] = [array[i + 1], array[i]];
        lastSwap = i;
      }
    }

    right = lastSwap;

    for (let i = right; i > left; i--) {
      if (array[i] < array[i - 1]) {
        [array[i], array[i - 1]] = [array[i - 1], array[i]];
        firstSwap = i;
      }
    }

    left = firstSwap;
  }

  return array;
}
