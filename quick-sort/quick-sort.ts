function partition(array, left, right) {
  const pivot = array[Math.floor((left + right) / 2)];

  while (left <= right) {
    while (array[left] < pivot) {
      left++;
    }

    while (array[right] > pivot) {
      right--;
    }

    if (left <= right) {
      [array[left], array[right]] = [array[right], array[left]];
      left++;
      right--;
    }
  }

  return left;
}

function sortNormal(array, left, right) {
  if (array.length > 1) {
    const index = partition(array, left, right);

    if (left < index - 1) {
      sortNormal(array, left, index - 1);
    }

    if (index < right) {
      sortNormal(array, index, right);
    }
  }

  return array;
}

function sortSimplified(array: number[]) {
  if (array.length < 2) {
    return array;
  }

  const pivot = array[Math.floor(array.length / 2)];
  const less = array.filter(val => val < pivot);
  const greater = array.filter(val => val > pivot);
  const equal = array.filter(val => val === pivot);

  return [
    ...sortSimplified(less),
    ...equal,
    ...sortSimplified(greater)
  ];
}
