function sortNormal(array: number[]) {
  let i = 1;

  while (i < array.length) {
    if (i > 0 && array[i] < array[i - 1]) {
      [array[i], array[i - 1]] = [array[i - 1], array[i]];
      i--;
    } else {
      i++;
    }
  }

  return array;
}
